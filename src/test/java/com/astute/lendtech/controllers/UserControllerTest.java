package com.astute.lendtech.controllers;

import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoginRequest;
import com.astute.lendtech.models.requests.UserRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-integration-tests.properties")
@ActiveProfiles(profiles = {"integration-tests"})
public class UserControllerTest {

    @Autowired
    Environment environment;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void handle_user_registration_success(){

        ResponseEntity<ResponseBody> response = restTemplate.exchange("/api/v1/users/register",
                HttpMethod.POST, getHttpEntity(userRequest()), ResponseBody.class);

        Assertions.assertEquals(201, Objects.requireNonNull(response.getBody()).getStatusCode());

    }

    @Test
    void handle_user_registration_fail(){

        ResponseEntity<ResponseBody> response = restTemplate.exchange("/api/v1/users/register",
                HttpMethod.POST, getHttpEntity(userRequestInvalid()), ResponseBody.class);

        Assertions.assertEquals(500, Objects.requireNonNull(response.getBody()).getStatusCode());

    }

    @Test
    void handle_user_login_success(){

        User user = new User();
        user.setEmail("allan@mail.com");
        user.setPassword(passwordEncoder.encode("password"));
        user.setPhoneNumber("0722123456");
        user.setLastName("Otieno");
        user.setFirstName("Allan");

        userRepository.save(user);

        ResponseEntity<ResponseBody> response = restTemplate.exchange("/api/v1/login",
                HttpMethod.POST, getHttpEntity(loginRequest()), ResponseBody.class);

        Assertions.assertEquals(200, Objects.requireNonNull(response.getBody()).getStatusCode());

    }

    @Test
    void handle_user_login_fail(){

        ResponseEntity<ResponseBody> response = restTemplate.exchange("/api/v1/login",
                HttpMethod.POST, getHttpEntity(loginRequest()), ResponseBody.class);

        Assertions.assertEquals(401, Objects.requireNonNull(response.getBody()).getStatusCode());

    }

    public HttpEntity getHttpEntity(Object object){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        return object == null ? new HttpEntity<>(headers) : new HttpEntity<>(object, headers);
    }

    public HttpEntity getHttpEntity(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(headers);
    }

    public UserRequest userRequest(){
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("allan@mail.com");
        userRequest.setFirstName("Allan");
        userRequest.setLastName("Otieno");
        userRequest.setPassword("password");

        return  userRequest;
    }

    public UserRequest userRequestInvalid(){
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("Allan");
        userRequest.setLastName("Otieno");
        userRequest.setPassword("password");

        return  userRequest;
    }

    public LoginRequest loginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("allan@mail.com");
        loginRequest.setPassword("password");

        return loginRequest;
    }




}
