package com.astute.lendtech.services.impl;

import com.astute.lendtech.models.requests.UserRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-integration-tests.properties")
@ActiveProfiles(profiles = {"integration-tests"})
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void registerUserPass(){
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("allan@mail.com");
        userRequest.setPassword(passwordEncoder.encode("password"));
        userRequest.setPhoneNumber("0722123456");
        userRequest.setLastName("Otieno");
        userRequest.setFirstName("Allan");

        ResponseBody user = userService.createUser(userRequest);

        Assertions.assertEquals(201, user.getStatusCode());

    }

    @Test
    void registerUserFail(){
        UserRequest userRequest = new UserRequest();
        userRequest.setPassword(passwordEncoder.encode("password"));
        userRequest.setPhoneNumber("0722123456");
        userRequest.setLastName("Otieno");
        userRequest.setFirstName("Allan");

        ResponseBody user = userService.createUser(userRequest);

        Assertions.assertEquals(500, user.getStatusCode());

    }


}
