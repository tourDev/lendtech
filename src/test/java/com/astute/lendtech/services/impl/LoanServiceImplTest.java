package com.astute.lendtech.services.impl;

import com.astute.lendtech.entitites.LoanWallet;
import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoanRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.LoanWalletRepository;
import com.astute.lendtech.repositories.UserRepository;
import com.astute.lendtech.services.LoanService;
import com.astute.lendtech.services.UserService;
import com.astute.lendtech.utils.StaticVariables;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-integration-tests.properties")
@ActiveProfiles(profiles = {"integration-tests"})
public class LoanServiceImplTest {

    @Autowired
    private LoanService loanService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoanWalletRepository loanWalletRepository;


    @Test
    void recordLoanPass(){
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setAmount(200);
        loanRequest.setCurrency(StaticVariables.CURRENCY_KES);
        loanRequest.setLender("Deogratius");
        loanRequest.setDisbursementStatus(StaticVariables.TRX_DISBURSEMENT);
        loanRequest.setRepaymentStatus(StaticVariables.REPAYMENT_STATUS_PENDING);

        User userRequest = new User();
        userRequest.setEmail("allan@mail.com");
        userRequest.setPassword(passwordEncoder.encode("password"));
        userRequest.setPhoneNumber("0722123456");
        userRequest.setLastName("Otieno");
        userRequest.setFirstName("Allan");

        User user = userRepository.save(userRequest);

        LoanWallet loanWallet = new LoanWallet();
        loanWallet.setUser(user);
        loanWallet.setCurrency(StaticVariables.CURRENCY_KES);
        loanWallet.setBalance(0);

        loanWalletRepository.save(loanWallet);

        ResponseBody loan = loanService.recordLoan(user, loanRequest);

        Assertions.assertEquals(201, loan.getStatusCode());

    }


}
