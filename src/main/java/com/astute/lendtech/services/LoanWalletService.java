package com.astute.lendtech.services;

import com.astute.lendtech.models.responses.ResponseBody;

import javax.servlet.http.HttpServletRequest;

public interface LoanWalletService {

    ResponseBody getLoanWallet(HttpServletRequest httpServletRequest);
}
