package com.astute.lendtech.services;

import com.astute.lendtech.models.requests.LoginRequest;
import com.astute.lendtech.models.requests.UserRequest;
import com.astute.lendtech.models.responses.ResponseBody;

public interface UserService {

    ResponseBody createUser(UserRequest userRequest);
    
    ResponseBody loginUser(LoginRequest loginRequest);

}
