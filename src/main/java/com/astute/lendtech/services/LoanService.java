package com.astute.lendtech.services;

import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoanRequest;
import com.astute.lendtech.models.responses.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public interface LoanService {

    ResponseBody recordLoan(User user, LoanRequest loanRequest);

    ResponseBody getLoans(HttpServletRequest httpServletRequest, Date startDate, Date endDate);

    ResponseBody repayLoan(HttpServletRequest httpServletRequest, Long loanID);
}
