package com.astute.lendtech.services.impl;

import com.astute.lendtech.entitites.LoanWallet;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.LoanWalletRepository;
import com.astute.lendtech.repositories.UserRepository;
import com.astute.lendtech.services.LoanWalletService;
import com.astute.lendtech.utils.StaticVariables;
import com.astute.lendtech.utils.UserMisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class LoanWalletServiceImpl implements LoanWalletService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoanWalletRepository loanWalletRepository;

    @Autowired
    UserMisc userMisc;

    @Override
    public ResponseBody getLoanWallet(HttpServletRequest httpServletRequest) {

        ResponseBody responseBody = new ResponseBody();

        Long userID = userMisc.getUserID(httpServletRequest).getId();

        if (userID == null){
            responseBody.setStatusMessage(StaticVariables.USER_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.USER_NOT_FOUND));
            return responseBody;
        }

        LoanWallet loanWallet = loanWalletRepository.findLoanWalletByUserId(userID);

        if (loanWallet == null){
            responseBody.setStatusMessage(StaticVariables.LOAN_WALLET_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.LOAN_WALLET_NOT_FOUND));
            return responseBody;
        }

        responseBody.setData(loanWallet);
        responseBody.setSuccess(true);
        responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_200);
        responseBody.setStatusMessage(StaticVariables.LOAN_WALLET_FOUND);

        return responseBody;
    }
}
