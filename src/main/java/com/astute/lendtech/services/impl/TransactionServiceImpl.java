package com.astute.lendtech.services.impl;

import com.astute.lendtech.entitites.Transaction;
import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.TransactionRepository;
import com.astute.lendtech.services.TransactionService;
import com.astute.lendtech.utils.StaticVariables;
import com.astute.lendtech.utils.UserMisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserMisc userMisc;

    @Override
    public ResponseBody getTransactions(HttpServletRequest httpServletRequest) {
        ResponseBody responseBody = new ResponseBody();

        User user = userMisc.getUserID(httpServletRequest);

        if (user == null){
            responseBody.setStatusMessage(StaticVariables.USER_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.USER_NOT_FOUND));
            return responseBody;
        }

        try {
            List<Transaction> transactions = transactionRepository.findAllByUser(user);

            responseBody.setData(transactions);
            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_200);
            responseBody.setStatusMessage(StaticVariables.TRANSACTION_FOUND);

        } catch (Exception exception) {
            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.ERROR_FETCHING_TRANSACTIONS);
        }

        return responseBody;
    }
}
