package com.astute.lendtech.services.impl;

import com.astute.lendtech.entitites.LoanWallet;
import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoginRequest;
import com.astute.lendtech.models.requests.UserRequest;
import com.astute.lendtech.models.responses.JwtResponse;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.LoanWalletRepository;
import com.astute.lendtech.repositories.UserRepository;
import com.astute.lendtech.security.JwtUtils;
import com.astute.lendtech.services.UserService;
import com.astute.lendtech.utils.LogHelper;
import com.astute.lendtech.utils.StaticVariables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final LoanWalletRepository loanWalletRepository;

    final LogHelper.LogBuilder logBuilder = LogHelper.builder(log)
            .operationName(StaticVariables.OP_API_REQUEST)
            .targetSystem(StaticVariables.TARGET_SYSTEM_WC)
            .logMsgType(StaticVariables.OP_REQUEST_TYPE)
            .logStatus(StaticVariables.STATUS_PROCESSING);

    public UserServiceImpl(UserRepository userRepository, LoanWalletRepository loanWalletRepository) {
        this.userRepository = userRepository;
        this.loanWalletRepository = loanWalletRepository;
    }

    @Override
    public ResponseBody createUser(UserRequest userRequest) {
        ResponseBody responseBody = new ResponseBody();

        try {
            User user = new User();
            user.setFirstName(userRequest.getFirstName());
            user.setLastName(userRequest.getLastName());
            user.setEmail(userRequest.getEmail());
            user.setPhoneNumber(userRequest.getPhoneNumber());
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));

            user = userRepository.save(user);

            // create user loan wallet balance
            LoanWallet loanWallet = new LoanWallet();
            loanWallet.setUser(user);
            loanWallet.setCurrency(StaticVariables.CURRENCY_KES);
            loanWallet.setBalance(0);

            loanWalletRepository.save(loanWallet);

            responseBody.setData(user);
            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_201);
            responseBody.setStatusMessage(StaticVariables.USER_CREATED_SUCCESSFULLY);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.USER_REGISTER_END)
                    .logMsgType(StaticVariables.OP_RESPONSE_TYPE)
                    .info();

        } catch (Exception exception){

            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.ERROR_CREATING_USER);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.USER_REGISTER_END)
                    .logDetailedMsg(exception.getMessage())
                    .error();

        }

        return responseBody;
    }

    @Override
    public ResponseBody loginUser(LoginRequest loginRequest) {
        ResponseBody responseBody = new ResponseBody();

        User user = userRepository.findByEmail(loginRequest.getEmail());

        if (user == null) {
            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_401);
            responseBody.setStatusMessage(StaticVariables.INCORRECT_CREDENTIALS);

            return responseBody;
        }

        try {
            Authentication authentication;

            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            String jwt = "";

            jwt = jwtUtils.generateJwtToken(userDetails, false);

            JwtResponse jwtResponse = JwtResponse.builder()
                    .token(jwt)
                    .email(user.getEmail())
                    .id(user.getId())
                    .type(StaticVariables.TOKEN_TYPE_BEARER)
                    .build();

            responseBody.setData(jwtResponse);
            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_200);
            responseBody.setStatusMessage(StaticVariables.LOGIN_SUCCESSFUL);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.USER_LOGIN_END)
                    .logMsgType(StaticVariables.OP_RESPONSE_TYPE)
                    .info();

        }catch (Exception e) {

            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.ERROR_LOGIN);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.USER_LOGIN_END)
                    .logDetailedMsg(e.getMessage())
                    .error();
        }

        return responseBody;
    }
}
