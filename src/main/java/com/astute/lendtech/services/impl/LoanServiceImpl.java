package com.astute.lendtech.services.impl;

import com.astute.lendtech.entitites.Loan;
import com.astute.lendtech.entitites.LoanWallet;
import com.astute.lendtech.entitites.Transaction;
import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoanRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.repositories.LoanRepository;
import com.astute.lendtech.repositories.LoanWalletRepository;
import com.astute.lendtech.repositories.TransactionRepository;
import com.astute.lendtech.services.LoanService;
import com.astute.lendtech.utils.LogHelper;
import com.astute.lendtech.utils.StaticVariables;
import com.astute.lendtech.utils.UserMisc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class LoanServiceImpl implements LoanService {

    @Autowired
    LoanRepository loanRepository;

    @Autowired
    LoanWalletRepository loanWalletRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserMisc userMisc;

    final LogHelper.LogBuilder logBuilder = LogHelper.builder(log)
            .operationName(StaticVariables.OP_API_REQUEST)
            .targetSystem(StaticVariables.TARGET_SYSTEM_WC)
            .logMsgType(StaticVariables.OP_REQUEST_TYPE)
            .logStatus(StaticVariables.STATUS_PROCESSING);

    @Override
    public ResponseBody recordLoan(User user, LoanRequest loanRequest) {
        ResponseBody responseBody = new ResponseBody();


        if (user == null){
            responseBody.setStatusMessage(StaticVariables.USER_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.USER_NOT_FOUND));
            return responseBody;
        }

        LoanWallet loanWallet = loanWalletRepository.findLoanWalletByUserId(user.getId());

        try {
            Loan loan = new Loan();
            loan.setAmount(loanRequest.getAmount());
            loan.setCurrency(loanRequest.getCurrency());
            loan.setDisbursementStatus(loanRequest.getDisbursementStatus());
            loan.setRepaymentStatus(loanRequest.getRepaymentStatus());
            loan.setLender(loanRequest.getLender());
            loan.setUser(user);

            loanRepository.save(loan);

            // save transaction in transaction table
            Transaction transaction = new Transaction();
            transaction.setAmount(loanRequest.getAmount());
            transaction.setUser(user);
            transaction.setTransactionDirection(StaticVariables.DIRECTION_CR);
            transaction.setTransactionType(StaticVariables.TRX_DISBURSEMENT);
            transaction.setLoan(loan);

            transactionRepository.save(transaction);

            // increment loan wallet balance
            double walletBalance = loanWallet.getBalance();
            double newWalletBalance = walletBalance + loanRequest.getAmount();
            loanWallet.setBalance(newWalletBalance);

            loanWalletRepository.save(loanWallet);

            responseBody.setData(loan);
            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_201);
            responseBody.setStatusMessage(StaticVariables.LOAN_RECORD_SUCCESS);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.LOAN_RECORD_SUCCESS)
                    .logMsgType(StaticVariables.OP_RESPONSE_TYPE)
                    .info();

        } catch (Exception exception) {
            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.LOAN_RECORD_FAILED);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.LOAN_RECORD_FAILED)
                    .logDetailedMsg(exception.getMessage())
                    .error();
        }

        return responseBody;
    }

    @Override
    public ResponseBody getLoans(HttpServletRequest httpServletRequest, Date startDate, Date endDate) {
        ResponseBody responseBody = new ResponseBody();

        User user = userMisc.getUserID(httpServletRequest);

        if (user == null){
            responseBody.setStatusMessage(StaticVariables.USER_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.USER_NOT_FOUND));
            return responseBody;
        }

        try {

            if (startDate != null){
                if (endDate == null) {
                    Date newEndDate = new Date();
                    List<Loan> loans = loanRepository.findAllByUserAndCreatedAtBetween(user, startDate, newEndDate);
                    responseBody.setData(loans);

                } else {
                    List<Loan> loans = loanRepository.findAllByUserAndCreatedAtBetween(user, startDate, endDate);
                    responseBody.setData(loans);

                }

            } else {

                List<Loan> loans = loanRepository.findAllByUser(user);
                responseBody.setData(loans);
            }

            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_200);
            responseBody.setStatusMessage(StaticVariables.LOAN_FOUND);

        } catch (Exception exception) {
            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.LOAN_FETCH_ERROR);
        }

        return responseBody;
    }

    @Override
    public ResponseBody repayLoan(HttpServletRequest httpServletRequest, Long loanID) {

        ResponseBody responseBody = new ResponseBody();

        User user = userMisc.getUserID(httpServletRequest);

        if (user == null){
            responseBody.setStatusMessage(StaticVariables.USER_NOT_FOUND);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
            responseBody.setSuccess(false);
            responseBody.setErrors(List.of(StaticVariables.USER_NOT_FOUND));
            return responseBody;
        }

        try {
            Loan loan = loanRepository.findByUserAndIdAndRepaymentStatus(user, loanID, StaticVariables.REPAYMENT_STATUS_PENDING);

            if (loan == null){
                responseBody.setSuccess(false);
                responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_404);
                responseBody.setStatusMessage(StaticVariables.LOAN_NOT_FOUND);

                return responseBody;
            }

            loan.setRepaymentStatus(StaticVariables.REPAYMENT_STATUS_PAID);
            loanRepository.save(loan);

            Transaction transaction = new Transaction();
            transaction.setAmount(loan.getAmount());
            transaction.setUser(user);
            transaction.setTransactionDirection(StaticVariables.DIRECTION_DR);
            transaction.setTransactionType(StaticVariables.TRX_REPAYMENT);
            transaction.setLoan(loan);

            transactionRepository.save(transaction);

            LoanWallet loanWallet = loanWalletRepository.findLoanWalletByUserId(user.getId());

            double walletBalance = loanWallet.getBalance();
            double newWalletBalance = walletBalance - loan.getAmount();
            loanWallet.setBalance(newWalletBalance);

            responseBody.setData(loan);
            responseBody.setSuccess(true);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_201);
            responseBody.setStatusMessage(StaticVariables.LOAN_PAID_SUCCESS);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.LOAN_PAID_SUCCESS)
                    .logMsgType(StaticVariables.OP_RESPONSE_TYPE)
                    .info();

        } catch (Exception exception) {
            responseBody.setSuccess(false);
            responseBody.setStatusCode(StaticVariables.RESPONSE_CODE_500);
            responseBody.setStatusMessage(StaticVariables.LOAN_PAID_ERROR);

            logBuilder.resetTime()
                    .logStatus(StaticVariables.STATUS_FINISHED)
                    .logMsg(StaticVariables.LOAN_PAID_ERROR)
                    .logDetailedMsg(exception.getMessage())
                    .error();
        }

        return responseBody;

    }
}
