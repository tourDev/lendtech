package com.astute.lendtech.models.responses;

import lombok.Data;

import java.util.List;

@Data
public class ResponseBody {

    private Boolean success;
    private List<String> errors;
    private int statusCode;
    private String statusMessage;
    private Object data;

}
