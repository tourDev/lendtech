package com.astute.lendtech.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {

    @Column(nullable = false)
    private double amount;

    @Column(nullable = false)
    private String lender;

    @Column(nullable = false)
    private String currency;

    @Column(nullable = false)
    private String disbursementStatus;

    @Column(nullable = false)
    private String repaymentStatus;

    private Long user_id;

}
