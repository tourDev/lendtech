package com.astute.lendtech.models.requests;

import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.Email;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {

    @Email
    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

}
