package com.astute.lendtech.controllers;

import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.services.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/v1")
public class TransactionController {

    private final TransactionService transactionService;


    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("transactions")
    public ResponseEntity<ResponseBody> getTransactions(HttpServletRequest httpServletRequest){
        return ResponseEntity.ok(transactionService.getTransactions(httpServletRequest));
    }
}
