package com.astute.lendtech.controllers;

import com.astute.lendtech.entitites.User;
import com.astute.lendtech.models.requests.LoanRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.services.LoanService;
import com.astute.lendtech.utils.LogHelper;
import com.astute.lendtech.utils.StaticVariables;
import com.astute.lendtech.utils.UserMisc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

@RestController
@RequestMapping("api/v1")
@Slf4j
public class LoanController {

    private final LoanService loanService;

    @Autowired
    UserMisc userMisc;

    final LogHelper.LogBuilder logBuilder = LogHelper.builder(log)
            .operationName(StaticVariables.OP_API_REQUEST)
            .targetSystem(StaticVariables.TARGET_SYSTEM_WC)
            .logMsgType(StaticVariables.OP_REQUEST_TYPE)
            .logStatus(StaticVariables.STATUS_PROCESSING);

    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @PostMapping("loans")
    public ResponseEntity<ResponseBody> recordLoan(HttpServletRequest httpServletRequest,
                                                   @RequestBody @Validated LoanRequest request){
        logBuilder.resetTime().logStatus(StaticVariables.STATUS_STARTING).logMsg(StaticVariables.LOAN_RECORD_START).info();

        User user = userMisc.getUserID(httpServletRequest);

        return ResponseEntity.ok(loanService.recordLoan(user, request));
    }

    @GetMapping("loans")
    public ResponseEntity<ResponseBody> getLoans(HttpServletRequest httpServletRequest,
                                                 @RequestParam(required = false) Date startDate,
                                                 @RequestParam(required = false) Date endDate){
        return ResponseEntity.ok(loanService.getLoans(httpServletRequest, startDate, endDate));
    }

    @PutMapping("loans/{loanID}")
    public ResponseEntity<ResponseBody> repayLoan(HttpServletRequest httpServletRequest,
                                                  @PathVariable @Validated Long loanID){
        logBuilder.resetTime().logStatus(StaticVariables.STATUS_STARTING).logMsg(StaticVariables.LOAN_UPDATE_START).info();
        return ResponseEntity.ok(loanService.repayLoan(httpServletRequest, loanID));
    }

}
