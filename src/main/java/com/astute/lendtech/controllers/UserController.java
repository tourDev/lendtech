package com.astute.lendtech.controllers;

import com.astute.lendtech.models.requests.LoginRequest;
import com.astute.lendtech.models.requests.UserRequest;
import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.security.AuthTokenFilter;
import com.astute.lendtech.security.JwtUtils;
import com.astute.lendtech.services.UserService;
import com.astute.lendtech.utils.LogHelper;
import com.astute.lendtech.utils.StaticVariables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/")
@Slf4j
public class UserController {

    @Autowired
    AuthTokenFilter authTokenFilter;

    @Autowired
    JwtUtils jwtUtils;

    private final UserService userService;

    final LogHelper.LogBuilder logBuilder = LogHelper.builder(log)
            .operationName(StaticVariables.OP_API_REQUEST)
            .targetSystem(StaticVariables.TARGET_SYSTEM_WC)
            .logMsgType(StaticVariables.OP_REQUEST_TYPE)
            .logStatus(StaticVariables.STATUS_PROCESSING);

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("users/register")
    public ResponseEntity<ResponseBody> createUser(@RequestBody @Validated UserRequest request) {
        logBuilder.resetTime().logStatus(StaticVariables.STATUS_STARTING).logMsg(StaticVariables.USER_REGISTER_START).info();
        return ResponseEntity.ok(userService.createUser(request));
    }

    @PostMapping("login")
    public ResponseEntity<ResponseBody> login(@RequestBody @Validated LoginRequest request){
        logBuilder.resetTime().logStatus(StaticVariables.STATUS_STARTING).logMsg(StaticVariables.USER_LOGIN_START).info();
        return ResponseEntity.ok(userService.loginUser(request));
    }

}
