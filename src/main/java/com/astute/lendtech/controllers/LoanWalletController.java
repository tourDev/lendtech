package com.astute.lendtech.controllers;

import com.astute.lendtech.models.responses.ResponseBody;
import com.astute.lendtech.services.LoanWalletService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/v1")
public class LoanWalletController {

    private final LoanWalletService loanWalletService;

    public LoanWalletController(LoanWalletService loanWalletService) {
        this.loanWalletService = loanWalletService;
    }

    @GetMapping("wallet")
    public ResponseEntity<ResponseBody> getLoanWalletBalance(HttpServletRequest httpServletRequest){

        return ResponseEntity.ok(loanWalletService.getLoanWallet(httpServletRequest));
    }
}
