package com.astute.lendtech.security;

import com.astute.lendtech.services.impl.UserDetailsImpl;
import com.astute.lendtech.utils.LogHelper;
import com.astute.lendtech.utils.StaticVariables;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class JwtUtils {

    @Value("${jwtSecretKey}")
    private String jwtSecret;

    final LogHelper.LogBuilder logBuilder = LogHelper.builder(log)
            .operationName(StaticVariables.OP_API_REQUEST)
            .targetSystem(StaticVariables.TARGET_SYSTEM_WC)
            .logMsgType(StaticVariables.OP_REQUEST_TYPE)
            .logStatus(StaticVariables.STATUS_PROCESSING);

    @Value("${jwtExpirationMs}")
    private Long jwtExpirationMs;

    public String generateJwtToken(UserDetailsImpl userPrincipal, Boolean isPermanent) {
        return generateTokenFromUsername(userPrincipal.getUsername(), isPermanent);
    }
    public String generateTokenFromUsername(String username, Boolean isPermanent) {
        if(isPermanent){
            jwtExpirationMs = jwtExpirationMs * 365 * 7;
        }
        return Jwts.builder().setSubject(username).setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs)).signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logBuilder.resetTime().logStatus(StaticVariables.STATUS_FINISHED).logMsg("Invalid JWT signature").logDetailedMsg( e.getMessage()).error();
        } catch (MalformedJwtException e) {
            logBuilder.resetTime().logStatus(StaticVariables.STATUS_FINISHED).logMsg("Invalid JWT token").logDetailedMsg( e.getMessage()).error();
        } catch (ExpiredJwtException e) {
            logBuilder.resetTime().logStatus(StaticVariables.STATUS_FINISHED).logMsg("JWT token is expired").logDetailedMsg( e.getMessage()).error();
        } catch (UnsupportedJwtException e) {
            logBuilder.resetTime().logStatus(StaticVariables.STATUS_FINISHED).logMsg("JWT token is unsupported").logDetailedMsg( e.getMessage()).error();
        } catch (IllegalArgumentException e) {
            logBuilder.resetTime().logStatus(StaticVariables.STATUS_FINISHED).logMsg("JWT claims string is empty").logDetailedMsg( e.getMessage()).error();

        }
        return false;
    }

}
