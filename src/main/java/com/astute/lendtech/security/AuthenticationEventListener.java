package com.astute.lendtech.security;

import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component("authenticationEventListener")
public class AuthenticationEventListener implements AuthenticationEventPublisher {

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
    }
    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {

        throw new BadCredentialsException("Invalid Username and Password");
    }
}
