package com.astute.lendtech.repositories;

import com.astute.lendtech.entitites.LoanWallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanWalletRepository extends CrudRepository<LoanWallet, Long> {


    LoanWallet findLoanWalletByUserId(Long user_id);
}
