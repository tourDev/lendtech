package com.astute.lendtech.repositories;

import com.astute.lendtech.entitites.Loan;
import com.astute.lendtech.entitites.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface LoanRepository extends CrudRepository<Loan, Long> {

    List<Loan> findAllByUser(User user);

    Loan findByUserAndId(User user, Long id);
    Loan findByUserAndIdAndRepaymentStatus(User user, Long id, String repaymentStatus);

    List<Loan> findAllByUserAndCreatedAtBetween(User user, Date startDate, Date endDate);

}
