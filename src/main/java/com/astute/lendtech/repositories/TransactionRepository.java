package com.astute.lendtech.repositories;

import com.astute.lendtech.entitites.Loan;
import com.astute.lendtech.entitites.Transaction;
import com.astute.lendtech.entitites.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> findAllByUser(User user);
}
