package com.astute.lendtech.utils;

import com.astute.lendtech.entitites.User;
import com.astute.lendtech.repositories.UserRepository;
import com.astute.lendtech.security.AuthTokenFilter;
import com.astute.lendtech.security.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserMisc {

    @Autowired
    AuthTokenFilter authTokenFilter;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserRepository userRepository;

    public User getUserID(HttpServletRequest httpServletRequest){

        String jwt = authTokenFilter.parseJwt(httpServletRequest);

        String email = jwtUtils.getUserNameFromJwtToken(jwt);

        User user = userRepository.findByEmail(email);

        if (user == null){
            return new User();

        }

        return user;
    }
}
