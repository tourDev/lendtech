package com.astute.lendtech.utils;

public class StaticVariables {

    private StaticVariables(){}

    public static final String SUCCESS = "Your request was processed successfully.";
    public static final String ERROR = "An error occurred while processing your request. Please try again.";
    public static final String LOAN_NOT_FOUND = "Loan not found.";
    public static final String USER_NOT_FOUND = "The requested item could not be found.";
    public static final String LOAN_FOUND = "Successfully fetched loans";
    public static final String LOAN_FETCH_ERROR = "An error occurred while fetching loans.";
    public static final String TRANSACTION_FOUND = "Successfully fetched transactions";
    public static final String ERROR_FETCHING_TRANSACTIONS = "An error occurred while fetching transactions.";
    public static final String DELETED = "The item was successfully deleted.";
    public static final int RESPONSE_CODE_200 = 200;
    public static final int RESPONSE_CODE_201 = 201;
    public static final int RESPONSE_CODE_500 = 500;
    public static final int RESPONSE_CODE_400 = 400;
    public static final int RESPONSE_CODE_401 = 401;
    public static final int RESPONSE_CODE_403 = 403;
    public static final int RESPONSE_CODE_404 = 404;

    public static final String DIRECTION_CR = "Cr";
    public static final String DIRECTION_DR = "DR";
    public static final String TRX_DISBURSEMENT = "Loan Disbursement";
    public static final String TRX_REPAYMENT = "Loan Payment";
    public static final String LOAN_RECORD_SUCCESS = "Successfully recorded loan.";
    public static final String LOAN_RECORD_FAILED = "An error occurred while recording loan transaction.";
    public static final String REPAYMENT_STATUS_PENDING = "pending";
    public static final String REPAYMENT_STATUS_PAID = "paid";
    public static final String LOAN_PAID_SUCCESS = "Loan successfully marked as paid.";
    public static final String LOAN_PAID_ERROR = "An error occurred while marking the loan as paid";
    public static final String LOAN_WALLET_NOT_FOUND = "Loan wallet not found";
    public static final String LOAN_WALLET_FOUND = "Loan wallet successfully fetched.";
    public static final String USER_NOT_FOUND_WITH_USERNAME = "User Not Found with username: ";

    public static final String CURRENCY_KES = "KES";
    public static final String USER_CREATED_SUCCESSFULLY = "Account created successfully.";
    public static final String ERROR_CREATING_USER = "An error occurred while creating user account.";
    public static final String INCORRECT_CREDENTIALS = "Incorrect credentials. Please try again.";
    public static final String LOGIN_SUCCESSFUL = "User Logged in successfully";
    public static final String ERROR_LOGIN = "An error occurred while logging in. Please try again.";
    public static final String TOKEN_TYPE_BEARER = "Bearer";

    public static final String OP_API_REQUEST = "User API Request";
    public static final String TARGET_SYSTEM_WC = "Web Client";
    public static final String OP_REQUEST_TYPE = "Request";
    public static final String OP_RESPONSE_TYPE = "Response";
    public static final String STATUS_PROCESSING = "Processing";
    public static final String STATUS_STARTING = "Starting";
    public static final String STATUS_FINISHED = "Finished";
    public static final String USER_REGISTER_START = "Received user register start request";
    public static final String USER_LOGIN_START = "Received user login start request";
    public static final String USER_LOGIN_END = "Finished user login request";
    public static final String USER_REGISTER_END = "Finished user register request";
    public static final String LOAN_RECORD_START = "Received loan record start request";
    public static final String LOAN_UPDATE_START = "Received loan update start request";
    public static final String LOAN_RECORD_END = "Finished loan record request";
    public static final String LOAN_UPDATE_END = "Finished loan update request";










}
