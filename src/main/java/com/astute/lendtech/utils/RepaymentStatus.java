package com.astute.lendtech.utils;

public enum RepaymentStatus {
    PENDING, PAID
}
