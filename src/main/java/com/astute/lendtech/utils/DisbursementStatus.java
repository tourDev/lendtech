package com.astute.lendtech.utils;

public enum DisbursementStatus {
    PENDING, APPROVED, CANCELLED
}
