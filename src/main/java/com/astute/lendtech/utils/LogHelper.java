package com.astute.lendtech.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Properties;
import java.util.function.Supplier;
import org.slf4j.Logger;

public class LogHelper {

    private LogHelper(){}

    @Accessors(fluent = true)
    public static class LogBuilder implements Cloneable {
        private final Logger logger;

        private StringBuilder stringBuilder;

        private String sourceSystem;

        private String sourceIP;

        @Setter
        private String operationName;

        @Setter
        private String targetSystem;

        @Setter
        private String targetEndpoint;

        @Setter
        private Supplier<String> targetEndpointFn;

        @Setter
        private String responseCode;

        @Setter
        private String logMsgType;

        @Setter
        private String logMsg;

        @Setter
        private String logDetailedMsg;

        @Setter
        private String logStatus;

        @Setter
        private String transactionID;

        private long startTime = System.currentTimeMillis();

        public LogBuilder(Logger logger) {
            this.logger = logger;

            if (sourceIP == null || sourceSystem == null) {
                try (final DatagramSocket socket = new DatagramSocket()) {
                    Properties properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("application.properties"));
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    sourceIP = socket.getLocalAddress().getHostAddress() + ":" + properties.getProperty("server.port");
                    sourceSystem = properties.getProperty("spring.application.name");

                } catch (IOException e) {
                    logger.error("LogMsg=Failed to load required properties for logger.");
                }
            }
        }

        private void build() {
            stringBuilder = new StringBuilder();
            stringBuilder.append("SourceSystem=").append(sourceSystem).append(" | SourceIP=").append(sourceIP);

            if (transactionID != null) stringBuilder.append(" | TransactionID=").append(transactionID);
            if (operationName != null) stringBuilder.append(" | OperationName=").append(operationName);
            if (targetSystem != null) stringBuilder.append(" | TargetSystem=").append(targetSystem);
            if (targetEndpoint != null) stringBuilder.append(" | TargetEndpoint=").append(targetEndpoint);
            if (targetEndpointFn != null) stringBuilder.append(" | ResponseCode=").append(responseCode);
            if (responseCode != null) stringBuilder.append(" | TransactionID=").append(transactionID);
            if (logMsgType != null) stringBuilder.append(" | LogMsgType=").append(logMsgType);
            if (logMsg != null) stringBuilder.append(" | LogMsg=").append(logMsg);
            if (logDetailedMsg != null) stringBuilder.append(" | LogDetailedMsg=").append(logDetailedMsg);
            if (logStatus != null) stringBuilder.append(" | LogStatus=").append(logStatus);
            stringBuilder.append(" | TransactionCost=").append(System.currentTimeMillis() - startTime);

        }

        public LogBuilder resetTime() {
            startTime = System.currentTimeMillis();
            return this;
        }

        public void error() {
            build();
            logger.error(stringBuilder.toString());
        }

        public void info() {
            build();
            logger.info(stringBuilder.toString());
        }
    }

    public static LogBuilder builder(Logger logger){
        return new LogBuilder(logger);
    }
}
