### LENDTECH DEMO
This is a sample demo project for recording loans, repayments and viewing various related transactions.
Postman Document Link: 
```
https://documenter.getpostman.com/view/2022839/UzBpKRAB
```
### PROJECT SETUP
To set up this project:
1. Fork/clone this repo to your local machine
2. Open it with your favourite IDE e.g. IntelliJ
3. Run mnv clean install to install all the dependencies
4. Create a database and update its credentials on the application.properties file
5. Run the project using mvn spring-boot:Run

### FRONTEND
Frontend Application for the service can be found on this link
```
https://gitlab.com/tourDev/lendtech-frontend
```


